import React from 'react';
import {Nav, NavItem, Navbar, Button, FormControl, Form} from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { logIn, logOut, signUp, showModal, closeModal, switchForm, showError, addOption, createdPoll, removeOption, editValue} from '../../actions/actions';
import jwtDecode from 'jwt-decode';
import { NavbarButtonsGroup } from '../NavbarButtonGroup.jsx';
import { CreateNewPollModal } from '../modals/CreateNewPollModal.jsx';
import { CreateNewUserModal } from '../modals/CreateNewUserModal.jsx';
import { LogInModal } from '../modals/LogInModal.jsx';


console.log('Navbar.jsx called');

let _NavbarComponent = React.createClass({
    componentDidMount() {
        this.props.CheckToken();
        // console.log('state:');
        // console.log(this.props.state);
        // console.log('local storage:');
        // console.log(window.localStorage);
    },
    render: function () {
        let modal;
        if (this.props.state.appReducer.showCreateNewUserModal) modal = <CreateNewUserModal />;
        if (this.props.state.appReducer.showCreateNewPollModal) modal = <CreateNewPollModal />;
        if (this.props.state.appReducer.showLogInModal) modal = <LogInModal />;
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <span id="home-link" href="/home">Vote.co</span>
                    </Navbar.Brand>
                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav bsStyle="pills">
                        <NavItem href="/home">
                            Home
                        </NavItem>

                        <NavItem href="/all">
                            All Polls
                        </NavItem>
                    </Nav>
                    <div className="pull-right navbar-buttons">


                        <NavbarButtonsGroup />

                        
                        <Form inline method="GET" action="/search" className="inline">
                            <FormControl type="search" name="q" placeholder="Search" ref="search-field" id="search-field"/>
                            <Button
                                type="submit"
                            >Search</Button>
                        </Form>
                    </div>
                </Navbar.Collapse>
                { modal }
            </Navbar>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        LogOut: () => {
            window.localStorage.removeItem('user_id');
            window.localStorage.removeItem('username');
            window.localStorage.removeItem('token');
            dispatch(logOut());
        },
        CheckToken: () => {
            if (window.localStorage.token !== undefined) {
                //check that the token is not expired
                //THIS IS NOT VERIFICATION!
                // Merely intended to not give a false sense of the user being logged in
                let now = Date.now();
                let expiryStamp;
                let decoded;
                decoded = jwtDecode(window.localStorage.token);
                expiryStamp = decoded.exp;
                // console.log('Expiry: '+expiryStamp );
                if (now > (expiryStamp*1000)) {
                    // console.log('token expired');
                    this.props.LogOut()
                }
                else if (now < (expiryStamp*1000)) {
                    dispatch(logIn());
                } 
            }
        }
    }
};

let NavbarComponent = connect(mapStateToProps, mapDispatchToProps)(_NavbarComponent);

module.exports = NavbarComponent;