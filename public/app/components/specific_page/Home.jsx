var React = require('react');
import { Row, Col} from 'react-bootstrap';
import { dataReceived, openPanel, showEditModal, hideEditModal, addOption, showConfirmModal, hideConfirmModal, retrievingData, retrievingDone, removeOption, editValue, resetReducer } from '../../actions/actions';
import { connect } from 'react-redux';
import { VotingPanel } from './../VotingPanel.jsx';

var _Home = React.createClass({
    componentDidMount() {
        this.retrieveRandomPoll();
    },
    retrieveRandomPoll () {
        if (!this.props.state.resultReducer.retrieving) {
            this.props.retrievingData();
            $.ajax({
                url: '/api?q=random',
                type: 'GET',
                success: (data) => {
                    // console.log('random poll found, data: ');
                    // console.log(data);
                    this.props.dataReceived(data);
                    this.props.retrievingDone();
                },
                error: (err) => {
                    console.log('could not retrieve user data');
                    console.log(err);
                }
            });
        }
    },
    render: function () {
        console.log('rendering Home');
        let data = this.props.state.resultReducer.data;
        console.log('data in home:');
        console.log(data);
        let panel;
        if (data.length !== 0) {
            panel = <VotingPanel data={data} index={0} key={'home-panel'} parent={'home'}/>
        } 
        return (
            <div id="home-container">
                <Row>
                    <Col xs={12} id="home-title">
                        <div id="main-title">
                            <h1>Welcome to the voting App</h1>
                        </div>
                        <div id="sub-title">
                            <h2>Random poll: </h2>
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={6} lgOffset={3}    >
                        {panel}
                    </Col>
                </Row>



            </div>
        )
    }
});


const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        dataReceived: (data) => {
            dispatch(dataReceived(data));
        },
        retrievingData: () => {
            dispatch(retrievingData());
        },
        retrievingDone: () => {
            dispatch(retrievingDone());
        }
    }
};

const Home = connect(mapStateToProps, mapDispatchToProps)(_Home);

module.exports = Home;