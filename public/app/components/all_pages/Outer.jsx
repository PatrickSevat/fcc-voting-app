import React from 'react';
import Navbar from './Navbar.jsx'
import {Row, Col} from 'react-bootstrap';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { appReducer } from '../../reducers/appReducer';
import { pollsReducer } from '../../reducers/pollsReducer';
import { resultReducer } from '../../reducers/resultReducer';

console.log('Outer.jsx called');

let combinedReducer = combineReducers({
    appReducer,
    pollsReducer,
    resultReducer
});

const store = createStore(combinedReducer);

var Outer = React.createClass({
    render: function () {
        return (

            <html>
            <head>
                <title>{this.props.title}</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="app.css"/>
            </head>
            <body>
            {/* Using the provider wrapper here and calling connect on the components that need it (check Navbar.jsx in my case) will pass the store down to the components that need it! 
             */}
            <Provider store={store}>
                <Row>
                    <Navbar />
                    {this.props.children}
                </Row>
            </Provider>
            {/*Remember, public folder is already set to static in server.js so no need to reference to public folder in src='' */}
            <script src='/bundle.js'></script>
            <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
            </body>
            </html>

        )
    }
});

module.exports = Outer;