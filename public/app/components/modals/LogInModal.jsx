import React from 'react';
import { Modal, Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import { hideLogInModal, showError, logIn } from '../../actions/actions';
import { WellAlert } from '../WellAlert.jsx';

const _LogInModal = React.createClass({
    submitLogIn() {
        let username = document.getElementById('loginUsername').value;
        let password = document.getElementById('loginPassword').value;
        let data = { 'username': username, 'password': password};
        $.ajax({
            url: '/authenticate',
            type: 'POST',
            data: data,
            success: (data) => {
                // console.log('login successful, data: ');
                // console.log(data);
                window.localStorage.setItem('user_id', data.user['_id']);
                window.localStorage.setItem('username', data.user.username);
                window.localStorage.setItem('token', data.token);
                this.props.LogIn();
                setTimeout(this.props.hideLogInModal, 1000);
            },
            error: (err) => {
                console.log('login failed: err');
                console.log(err);
                this.props.ShowError(err);
            }
        });
    },
    render: function () {
        // console.log('rendering LogIn Modal');
        let wellMessage;
        let wellStyle;
        if (this.props.state.appReducer.loggedIn) {
            wellMessage = 'Succesfully logged in';
            wellStyle= 'success';
            
        }
        else {
            wellMessage = 'Click here to sign up';
            wellStyle = 'info'
        }
        return (
            <Modal show={this.props.state.appReducer.showLogInModal} onHide={this.props.hideLogInModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <WellAlert wellMessage={wellMessage} wellStyle={wellStyle} showWell={true} />
                    <form id="login-form" >
                        <FormGroup>
                            <ControlLabel>Username</ControlLabel>
                            <FormControl type="text" id="loginUsername" name="username" />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Password</ControlLabel>
                            <FormControl type="password" id="loginPassword" name="password"/>
                        </FormGroup>
                        <Button
                            id="login-submit"
                            onClick={this.submitLogIn}
                        >
                            Submit
                        </Button>
                        <Button
                            onClick={this.props.hideLogInModal}
                        >
                            Close
                        </Button>

                    </form>
                </Modal.Body>
            </Modal>
        )
    }

});


const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        LogIn: () => {
            dispatch(logIn());
        },
        hideLogInModal: () => {
            dispatch(hideLogInModal())
        },
        ShowError: (err) => {
            dispatch(showError(err))
        }
    }
};

const LogInModal = connect(mapStateToProps, mapDispatchToProps)(_LogInModal);

exports.LogInModal = LogInModal;