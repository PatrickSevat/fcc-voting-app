import React from 'react';
import { Button, ButtonToolbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { logOut, showLogInModal, showCreateNewPollModal } from '../actions/actions';

const _NavbarButtonGroup = React.createClass({
    logOut () {
        window.localStorage.removeItem('user_id');
        window.localStorage.removeItem('username');
        window.localStorage.removeItem('token');
        this.props.logOut();  
    },
    render: function () {
        console.log('rendering Navbar button group');
        let buttons;
        let loggedIn = this.props.state.appReducer.loggedIn;
        if (loggedIn) {
            buttons =
                <ButtonToolbar className="inline-block">
                    <Button
                        onClick={this.props.showCreateNewPollModal}
                    >
                        Create new poll
                    </Button>
                    <Button href="/profile">
                        My profile
                    </Button>
                    <Button
                        onClick={this.logOut}
                    >
                        Log out
                    </Button>
                </ButtonToolbar>;
        }
        else {
            buttons = 
                <ButtonToolbar className="inline-block">
                    <Button
                        onClick={this.props.showLogInModal}
                    >
                        Log in
                    </Button>
                    <Button
                        onClick={this.props.showCreateNewPollModal}
                    >
                        Create new poll
                    </Button>
                </ButtonToolbar>
                ;
        }
        return (
            <Nav className="inline-block">{buttons}</Nav>

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => {
            dispatch(logOut());
        },
        showLogInModal: () => {
            dispatch(showLogInModal());
        },
        showCreateNewPollModal: () => {
            if (window.localStorage.token !== undefined) dispatch(showCreateNewPollModal());
            else dispatch(showLogInModal());
        }
    }
};

const NavbarButtonGroup = connect(mapStateToProps, mapDispatchToProps)(_NavbarButtonGroup);

exports.NavbarButtonsGroup = NavbarButtonGroup;