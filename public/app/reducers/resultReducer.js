/**
 * Created by Patrick on 21/07/2016.
 */
const initState = {
    openPanel: 0,
    //reset data to false in case of errors
    data: [],
    showEditModal: false,
    editModalData: {options: []},
    createdPoll: false,
    showConfirmModal: false,
    indexToDelete: false,
    retrieving: false
};

// Example of an Object in data array
// {
    // _v: 0,
    // _id:"5790e91c3cf860443aabb681",
    // creatorID:"578d46fab2a0761824f6bfda",
    // description:"descr1 edit3",
    // name:"poll1 edit4"
    // options: [
    //  {
    //  optionVotes:"9",
    //  value:"opt 2 edit3"
    // }
    // ]
// }

const resultReducer = (state = initState, action) => {
    switch (action.type) {
        case 'RESET-REDUCER':
            return initState;
        case 'OPEN-PANEL':
            if (state.openPanel === action.newPanel) {
                return Object.assign({}, state, {
                    openPanel: false
                });
            }
            else {
                return Object.assign({}, state, {
                    openPanel: action.newPanel
                });
            }
        case 'DATA-RECEIVED':
            return Object.assign({}, state, {
                data: action.data,
                createdPoll: false
            });
        case 'SHOW-EDIT-MODAL':
            return Object.assign({}, state, {
                showEditModal: true,
                editModalData: action.data
            });
        case 'HIDE-EDIT-MODAL':
            return Object.assign({}, state, {
                showEditModal: false,
                editModalData: false
            });
        case 'ADD-OPTION':
            let newModalData;
            if (state.editModalData) {
                newModalData = state.editModalData;
                newModalData.options.push({
                    value: '',
                    optionVotes: 0
                });
            }
            else return state;
            return Object.assign({}, state, {
                editModalData: newModalData
            });
        case 'REMOVE-OPTION':
            if (action.form === 'edit') {
                let optionsArr = state.editModalData.options;
                optionsArr = [...optionsArr.slice(0, action.index),
                                        ...optionsArr.slice(action.index+1)];
                let newEditModalData = Object.assign({}, state.editModalData, {
                        options: optionsArr
                });
                return Object.assign({}, state, {
                    editModalData: newEditModalData
                });

            }
            else return state;
        case 'EDIT-VALUE':
            if (action.form === 'edit') {
                let optionsArr2 = state.editModalData.options;
                optionsArr2[action.index].value = action.value;
                let newEditModalData2 = Object.assign({}, state.editModalData, {
                    options: optionsArr2
                });
                return Object.assign({}, state, {
                    editModalData: newEditModalData2
                });
            }
            else return state;
        case 'CREATED-POLL':
            // console.log('created poll in resultReducer');
            // console.log('new poll data in resultReducer')
            console.log('state.data in created poll in resultReducer');
            console.log(state.data);
            let newDataArr =[];
            if (state.data instanceof Array) {
                newDataArr = state.data;
                newDataArr.push(action.pollData);
            }
            else {
                newDataArr.push(state.data);
                newDataArr.push(action.pollData);
            }
            return Object.assign({}, state, {
                createdPoll: true,
                retrieving: false,
                data: newDataArr
            });
        case 'SHOW-CONFIRM-MODAL':
            return Object.assign({}, state, {
                showConfirmModal: true,
                indexToDelete: action.indexToDelete
            });
        case 'HIDE-CONFIRM-MODAL':
            return Object.assign({}, state, {
                showConfirmModal: false
            });
        case 'RETRIEVING-DATA':
            return Object.assign({}, state, {
                retrieving: true
            });
        case 'RETRIEVING-DONE':
            return Object.assign({}, state, {
                retrieving: false
            });
        case 'ADD-VOTE':
            let index;
            for (let i = 0; i< state.data.length; i++) {
                if (state.data._id === action.data._id){
                    index = i;
                    break;
                }
            }
            let newData = state.data;
            newData[index] = action.data;
            return Object.assign({}, state, {
                data: newData
            });
        default:
            return state
    }
};

exports.resultReducer = resultReducer;