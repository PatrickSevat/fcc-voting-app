/**
 * Created by Patrick on 20/07/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const PollSchema = new Schema({
    creatorID: {type: String, required: true},
    username: {type: String, required: true},
    name: {type: String, required: true},
    description: {type: String},
    options: {type: [Schema.Types.Mixed], required: true}
});


// PollSchema.statics.random = function (callback) {
//     this.count(function(err, count) {
//         if (err) {
//             return callback(err);
//         }
//         var rand = Math.floor(Math.random() * count);
//         this.findOne().skip(rand).exec(callback);
//     }.bind(this));
// };
exports.PollModel = mongoose.model('poll', PollSchema);