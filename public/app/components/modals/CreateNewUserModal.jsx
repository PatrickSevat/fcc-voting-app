import React from 'react';
import { Modal, Button, Well, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import { hideCreateNewUserModal, signUp, showError, showLogInModal } from '../../actions/actions';
import { WellAlert } from '../WellAlert.jsx';

const _CreateNewUserModal = React.createClass({
    createUser() {
        // console.log('createUser called');
        let username = document.getElementById('signupUsername').value;
        let password = document.getElementById('signupPassword').value;
        let email = document.getElementById('signupEmail').value;
        let data = { username, password, email};
        $.ajax({
            url: '/signup',
            type: 'POST',
            data: data,
            success: (data) => {
                // console.log('created user successful, data: ');
                // console.log(data);
                this.props.signedUp();
                setTimeout(this.props.showLogInModal, 1000);
            },
            error: (err) => {
                console.log('login failed: err');
                console.log(err);
                this.props.ShowError(err);
            }
        });
    },
    render: function () {
        let wellMessage;
        let wellStyle;
        if (this.props.state.appReducer.signedUp) {
            wellMessage = 'Succesfully signed up';
            wellStyle= 'success';
        }
        else {
            wellMessage = 'Click here to log-in with an existing account';
            wellStyle = 'info'
        }
        return (
            <Modal show={this.props.state.appReducer.showCreateNewUserModal} onHide={this.props.hideCreateNewUserModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Sign up</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <WellAlert wellMessage={wellMessage} wellStyle={wellStyle} showWell={true} />
                    <form id="signup-form" >
                        <FormGroup>
                            <ControlLabel>Username</ControlLabel>
                            <FormControl type="text" placeholder="Choose a username" id="signupUsername" name="username" />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Email address</ControlLabel>
                            <FormControl type="email" placeholder="Enter email" id="signupEmail" name="email" />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Password</ControlLabel>
                            <FormControl type="password" id="signupPassword" name="password"/>
                        </FormGroup>
                        <Button id="signup-submit"
                                onClick={this.createUser}
                        >
                            Submit
                        </Button>
                        <Button
                            onClick={this.props.hideCreateNewUserModal}
                        >
                            Close
                        </Button>

                    </form>
                </Modal.Body>
            </Modal>
        )
    }
    
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        signedUp: () => {
            dispatch(signUp());
        },
        hideCreateNewUserModal: () => {
            dispatch(hideCreateNewUserModal())
        },
        ShowError: (err) => {
            dispatch(showError(err))
        },
        showLogInModal: () => {
            dispatch(showLogInModal())
        }
    }
};


const CreateNewUserModal = connect(mapStateToProps, mapDispatchToProps)(_CreateNewUserModal );

exports.CreateNewUserModal = CreateNewUserModal; 