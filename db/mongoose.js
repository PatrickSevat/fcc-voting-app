/**
 * Created by Patrick on 16/07/2016.
 */
import mongoose from 'mongoose';
import {mLabsUrl} from './mLabsUrl'
mongoose.connect(mLabsUrl);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function () {
    console.log('mongo connected');

});

module.exports = db;