"use strict";
import express from 'express';
import {join, normalize } from 'path';
import favicon from 'serve-favicon';
import  ReactEngine  from 'react-engine';
import routes from './public/app/routes/Routes.jsx';
import db from './db/mongoose';
import { UserModel } from './db/users';
import { PollModel } from './db/polls';
import bodyParser from 'body-parser';
import { tokenSecret } from './config';
const jwt = require('jsonwebtoken');

//Define our app as an Express app
const app = express();

//Request body parser for requests and submissions
app.use(bodyParser.urlencoded({ extended: true }));


//Configuring Passport login methods
//local
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
app.use(passport.initialize());

passport.use(new LocalStrategy((username, password, done) => {
    UserModel.getAuthenticated(username, password, (err, user, reason) => {
        if (err) return done(err);
        if (user === null) return done(null, false, {message: reason});
        return done(null, user);
    })
}));


//Define the path to our React-Router file
const reactRoutesFilePath = join(__dirname, './public/app/routes/Routes.jsx');

//Define our React view and plug in the imported routes file and the filepath to routes
var engine = ReactEngine.server.create({
    routes: require(reactRoutesFilePath),
    routesFilePath: reactRoutesFilePath
});

//Setup our express engine for .jsx files
app.engine('.jsx', engine);

//Set up our views directory filepath
app.set('views', join(__dirname, './public/app/components'));

//Define our view engine
app.set('view engine', 'jsx');

//Set custom view
app.set('view', ReactEngine.expressView);

//Define our /public directory as a static -publicly accessible- directory
app.use(express.static('public'));

//Serve a favicon with each request
app.use(favicon(join(__dirname, './public/favicon.ico')));

//ACAO for client independent requests, define allowed methods and allowed headers
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

app.post('/authenticate',
    passport.authenticate('local',{
        session: false
    }), serialize, generateToken, respond);

function serialize(req,res,next) {
    req.user = req.user.toObject();
    delete req.user.password;
    next();
    /*
    * The serialize function is pretty much the core of the whole thing. It has basically three main jobs to do, two of them are only needed if you are using external authentication services:

     1. Create users, which are authenticated, but not in your database. This only happens if you use passport strategies for external services, like the Google or Facebook login (if someone authenticates with their google account they may have never been on your service and use it for the first time).
     2. Update the user data: Like #1, but now updates an already known user (a Facebook user could have switched his name in Facebook, you may want to update this in here too).
     3. Complete the user data in your req.user object: If you need additional information which aren't inside the authentication process, you can store it req.users (and therefore in your token) in here.
*/
}

function generateToken(req, res, next) {
    req.token = jwt.sign({
        id: req.user.id
    }, tokenSecret, {
        expiresIn: 2*60*60*1000
    });
    next();
}

function respond(req, res) {
    res.status(200).json({
        user: req.user,
        token: req.token,
        message: 'Log in successful'
    });
}

//Verify token middleware
const verify = (req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers.authorization;
    if (token) {
        jwt.verify(token, tokenSecret, (err, decoded) => {
            if (err) {
                return res.status(401).json({message: 'failed authentication'})
            }
            else {
                next()
            }
        })
    }
    else return res.status(401).json({message: 'failed authentication'})
};

//API routing
//app.post serves the C(reate) in CRUD
app.post('/api*', verify, function(req, res) {
    if (req.query.createPoll) {
        console.log('createPoll req.body:');
        console.log(req.body);
        let newPoll = new PollModel ({
            creatorID: req.body.creatorID,
            username: req.body.username,
            name: req.body.pollName,
            description: req.body.pollDescription,
            options: req.body.options
        });
        newPoll.save((err, savedDoc) => {
            if (err) {
                console.log(err);
                res.status(400).json({message: 'ERROR: '+err})
            }
            else {
                res.status(200).json({
                    message: 'poll created',
                    data: savedDoc
                })
            }
        })
    }
});

//app.get serves the R(ead) in CRUD, does not require a verify method
app.get('/api*', function(req, res) {
    //Check for an id in the url query
    if (req.query.userID) {
        console.log('req.query.userID = '+req.query.userID);
        //Query our Mongo\Mongoose db for the id
        PollModel.find({
            creatorID: req.query.userID
        }, function (err, results) {
            if (err) res.status(404).json({message: 'userID not found'});
            else res.status(200).send(results);
        })
    }
    else if (req.query.q) {
        console.log('req.query.q= '+req.query.q);
        //Query our Mongo\Mongoose db for all polls
        if (req.query.q === '') {
            PollModel.find({}, function (err, results) {
                if (err) res.status(404).json({message: 'No polls found'});
                else res.status(200).send(results);
            })
        }
        else if (req.query.q === 'random') {
            PollModel.count().exec(function(err, count){
                var random = Math.floor(Math.random() * count);
                PollModel.findOne().skip(random).exec(
                    function (err, result) {
                        if (err) res.status(404).json({message: 'Random retrieval failed'});
                        else res.status(200).send(result);
                    });
            });
        }
        else if (req.query.q !== undefined) {
            PollModel.find({
                // creatorID: req.query.userID
            }, function (err, results) {
                if (err) res.status(404).json({message: 'userID not found'});
                else res.status(200).send(results);
            })
        }

    }
});

//app.put serves the U(pdate) in CRUD
app.put('/api/vote', (req, res) => {
    //Check for an id in the url query
    if (req.query.pollID) {
        console.log('req.query.pollID = '+req.query.pollID);
        //Query our Mongo\Mongoose db for the id
        PollModel.findByIdAndUpdate(req.query.pollID, { $set: {
            options: req.body.options
        }
        }, {new : true}, function (err, result) {
            if (err) res.status(404).json({message: 'PollID not found'});
            else res.status(200).send(result);
        })
    }
});

app.put('/api*', verify, (req, res) => {
    //Check for an id in the url query
    if (req.query.pollID) {
        console.log('req.query.pollID = '+req.query.pollID);
        //Query our Mongo\Mongoose db for the id
        PollModel.findByIdAndUpdate(req.query.pollID, { $set: {
            name: req.body.pollName,
            description: req.body.pollDescription,
            options: req.body.options
            }
        }, {new : true}, function (err, result) {
            if (err) res.status(404).json({message: 'PollID not found'});
            else res.status(200).send(result);
        })
    }
});

//app.delete serves the D(elete) in CRUD
app.delete('/api*', verify, (req, res) => {
    if (req.query.pollID) {
        console.log('app.delete req.query.pollID = '+req.query.pollID);
        //Query our Mongo\Mongoose db for the id
        PollModel.remove({_id: req.query.pollID}, function (err, result) {
            if (err) res.status(404).json({message: 'PollID not found'});
            else res.status(200).json({message: 'Poll succesfully removed'});
        })
    }
});

app.post('/signup', (req, res, next) => {
    let newUser = new UserModel ({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    });
    newUser.save((err) => {
        if (err) {
            console.log(err);
            res.status(400).json({message: 'ERROR: '+err})
        }
        else {
            res.status(200).json({message: 'user created'})
        }

    })
});

app.get('/search', (req, res, next) => {
    console.log('search req.query.q');
    console.log(req.query.q);
    let query = req.query.q;
    let re = new RegExp('\w*'+query+'.*', 'i');

    PollModel.find({ $or: [{'name': re}, {'username': re}, {'description': re} ] }, (err, results) => {
        console.log('search results in server.js:');
        console.log(results);
        res.render(req.url, {
            title: "Search",
            data: results
        });
    } )
});

//All other routes
app.get('*', function(req, res) {
    //Render the HTML based on the req.url,
    // by using req.url React Engine uses React Router in the background
    res.render(req.path, {
        title: "Voting App"
    });
});

//ReactRouter error handling, send the correct HTML error codes if needed
app.use(function(err, req, res, next) {
    
    // http://expressjs.com/en/guide/error-handling.html
    if (res.headersSent) {
        return next(err);
    }

    if (err._type && err._type === ReactEngine.reactRouterServerErrors.MATCH_REDIRECT) {
        //console.log('ReactRouter error handling');
        //console.error(err);
        //console.log(req);
        return res.redirect(302, err.redirectLocation);
    }
    else if (err._type && err._type === ReactEngine.reactRouterServerErrors.MATCH_NOT_FOUND) {
        //console.log('ReactRouter error handling');
        //console.error(err);
        //console.log(req);
        return res.status(404).send('Route Not Found!');
    }
    else {
        // for ReactEngine.reactRouterServerErrors.MATCH_INTERNAL_ERROR or
        // any other error we just send the error message back
        //console.log('ReactRouter error handling');
        //console.error(err);
        //console.log(req);
        return res.status(500).send(err.message);
    }
});

const port = process.env.PORT || 3000;

//Start our web app
app.listen(port, function(){
    console.log('app listening on port 3000');
});


