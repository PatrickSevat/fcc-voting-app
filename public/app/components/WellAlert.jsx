import React from 'react';
import { Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import { showCreateNewUserModal, showLogInModal } from '../actions/actions';

const _WellAlert = React.createClass({
    render: function () {
        let well;
        if (this.props.showWell) {
            well =
                <Alert
                    bsStyle={this.props.wellStyle || ''}
                    onClick={() => {
                        if(!this.props.state.appReducer.loggedIn && this.props.state.appReducer.showLogInModal) {
                            this.props.showCreateNewUserModal()
                        }
                        if(!this.props.state.appReducer.signedUp && this.props.state.appReducer.showCreateNewUserModal) {
                            this.props.showLogInModal()
                        }
                    }} 
                >
                    {this.props.wellMessage}
                    </Alert>
        }
        return (
            <div>
                {well}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        showCreateNewUserModal: () => {
            dispatch(showCreateNewUserModal());
        },
        showLogInModal: () => {
            dispatch(showLogInModal());
        }
    }
};

const WellAlert = connect(mapStateToProps, mapDispatchToProps)(_WellAlert);

exports.WellAlert = WellAlert;