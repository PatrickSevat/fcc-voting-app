/**
 * Created by Patrick on 20/07/2016.
 */
const initState = {
    newPoll: false,
    newPollOptions: [{value: '', optionVotes: 0}]
};

const pollsReducer = (state = initState, action) => {
  switch (action.type) {
      case 'ADD-OPTION':
          let optionsArr1 = state.newPollOptions;
          optionsArr1.push({value: '', optionVotes: 0});
          return Object.assign({}, state, {
              newPollOptions: optionsArr1
          });
      case 'EDIT-VALUE':
          if (action.form === 'create') {
              let optionsArr2 = state.newPollOptions;
              optionsArr2[action.index].value = action.value;
              return Object.assign({}, state, {
                  newPollOptions: optionsArr2
              });    
          }
          else return state;
      case 'REMOVE-OPTION':
          if (action.form === 'create') {
              // console.log('remove opt in pollsreducer');
              // let optionsArr3 = [...state.newPollOptions.slice(0, action.index),
              //     ...state.newPollOptions.slice(action.index+1)];
              // return Object.assign({}, state, {
              //     newPollOptions: optionsArr3
              // });
              // let optionsArr =[];
              // if (action.optionsArr.length) optionsArr.push({value: ''});
              // else {
              //     for (let i=0; i < action.optionsArr.length; i++) {
              //         optionsArr.push({value: action.optionsArr[i]});
              //     }    
              // }
              //
              // return Object.assign({}, state, {
              //     newPollOptions: optionsArr
              // })
              let optionsArr = state.newPollOptions;
              optionsArr = [...optionsArr.slice(0, action.index),
                  ...optionsArr.slice(action.index+1)];
              return Object.assign({}, state, {
                  newPollOptions: optionsArr
              });
          }
          else return state;
      default:
          return state;
  }
};

exports.pollsReducer = pollsReducer;