const React = require('react');
import { Accordion, Col } from 'react-bootstrap';
import { dataReceived, openPanel, resetReducer } from '../../actions/actions';
import { connect } from 'react-redux';
import { VotingPanel } from './../VotingPanel.jsx'
import { EditPollModal } from './../modals/EditPollModal.jsx';
import { ConfirmDeletePollModal } from './../modals/ConfirmDeletePollModal.jsx';
import { WellAlert } from '../WellAlert.jsx';

const _All = React.createClass({
    componentDidMount() {
        //resetReducer is needed because resultReducer is also shared with Profile.jsx
        this.props.resetReducer();
        this.retrieveAllPolls();
    },
    retrieveAllPolls() {
        // console.log('retrieve all polls called');
        $.ajax({
            url: '/api?q=""',
            type: 'GET',
            success: (data) => {
                // console.log('user polls found, data: ');
                // console.log(data);
                this.props.dataReceived(data);
            },
            error: (err) => {
                console.log('could not retrieve user data');
                console.log(err);
            }
        });
    },
    render: function () {
        let data = this.props.state.resultReducer.data;
        let noPollsMessage;
        if (this.props.state.resultReducer.data.length === 0) {
            noPollsMessage = <WellAlert showWell={true} wellStyle={'info'} wellMessage={'You have not created any polls yet'}/>
        }
        let modal;
        if (this.props.state.resultReducer.showEditModal) modal = <EditPollModal />;
        if (this.props.state.resultReducer.showConfirmModal) modal = <ConfirmDeletePollModal />;
        return (
            <div id="allPolls-results-container">
                <div id="main-title">
                    <h1>All polls</h1>
                </div>
                <Col xs={12} sm={12} md={12} lg={6} lgOffset={3}    >

                    { noPollsMessage }
                    <Accordion>
                        {data.map((item, i) => {
                            return (
                                <VotingPanel data={item} index={i} key={'voting-panel-'+i} parent={'all'}/>
                            )
                        })}
                    </Accordion>
                    { modal }
                </Col>

            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetReducer: () => {
            dispatch(resetReducer());
        },
        dataReceived: (data) => {
            dispatch(dataReceived(data));
        },
        openPanel: (newPanel) => {
            dispatch(openPanel(newPanel));
        }
    }
};

let All = connect(mapStateToProps, mapDispatchToProps)(_All);

module.exports = All;