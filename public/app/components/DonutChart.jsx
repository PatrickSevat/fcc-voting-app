import React from 'react';
var d3 = require('d3');
var d3tooltip = require('d3-tooltip');
var ReactFauxDOM = require('react-faux-dom');


let DonutChart = React.createClass({
    componentDidMount () {
        if (typeof window !== 'undefined') {
            const chart = document.getElementById('chart');
            TweenMax.from(chart, 5, {
                opacity: 0,
                scale: 0,
                ease: Bounce.easeOut
            })
        }
    },
    render: function () {

        // console.log('rendering chart');
        const width = 260 ;
        const height = 260;
        let data = this.props.data.options;
        // console.log('data in DonutChart render:');
        // console.log(data)
        let index = this.props.index;

        var color = d3.scale.category20b();

        const arc = d3.svg.arc()
            .outerRadius(125)
            .innerRadius(100);

        const pie = d3.layout.pie()
            .sort(null)
            .value((d) => {
                return parseInt(d.optionVotes)
            });

        const node = ReactFauxDOM.createElement('svg');
        let svg = d3.select(node)
                .attr({
                     'width': width,
                    'height': height,
                    'id': 'chart'
                })
            .append('g')
                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        let g = svg.selectAll('.arc')
                .data(pie(data))
            .enter().append('g')
            .attr('class', 'arc');

        let path = g.append('path')
                .attr('d', arc)
                .style('fill', (d, i) => {
                    return color(i)
                });

        if (typeof window !== 'undefined') {
            let tooltip = d3tooltip(d3);
            path.on("mouseover", function(d) {
                    var html = 'Option: '+d.data.value+'<br>'+'Votes: '+d.data.optionVotes;
                    tooltip.html(html);
                    tooltip.show()
                })
                .on("mouseout", tooltip.hide);
        }


        return node.toReact()
    }
        
});

exports.DonutChart = DonutChart;