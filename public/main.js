/**
 * Created by Patrick on 18/07/2016.
 */

//require sass
require("./app.sass");


//Set up React Client
var Routes = require('./app/routes/Routes.jsx');
var Client = require('react-engine/lib/client');

// boot options
var options = {
    routes: Routes,

    // supply a function that can be called
    // to resolve the file that was rendered.
    viewResolver: function(viewName) {
        return require('./app/components/' + viewName);
    }
};

document.addEventListener('DOMContentLoaded', function onLoad() {
    Client.boot(options);
});

