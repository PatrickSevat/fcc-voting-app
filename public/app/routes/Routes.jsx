import React from 'react';
import { Router, Route, Redirect, IndexRoute, IndexRedirect, hashHistory } from 'react-router';


//Components here
import Outer from '../components/all_pages/Outer.jsx';
import Home from '../components/specific_page/Home.jsx';
import All from '../components/specific_page/All.jsx';
import Profile from '../components/specific_page/Profile.jsx';
import  Search from '../components/specific_page/Search.jsx';



//Normally you would wrap <Router/> in a <Provider store={reduxStore} /> wrapper
//However, that doesnt really work wel with React-Engine as the Outer component also contains <html><header>, ect.
//So the Provider wrapping was done in the Outer component, go check it out

var Routes = (
        <Router history={hashHistory}>
            <Redirect from="/gohome" to="/home" />
            <Route path="/" component={Outer}>
                <IndexRedirect to="/home"/>
                <Route path="home" component={Home} />
                <Route path="all" component={All} />
                <Route path="profile" component={Profile} />
                <Route path="search" component={Search} />
            </Route>
        </Router>

);

module.exports = Routes;