import React from 'react';
import { Button, ButtonToolbar } from 'react-bootstrap';
import { connect } from 'react-redux';
import { showConfirmModal, showEditModal } from '../actions/actions';

const _OwnerButtons = React.createClass({
    editPoll(i) {
        let pollData = this.props.state.resultReducer.data[i];
        this.props.showEditModal(pollData);
    },
    confirmDeletePoll(index) {
        this.props.showConfirmModal(index);
    },
    render: function () {
        console.log('rendering Owner Buttons');
        let buttons;
        if (this.props.userID === this.props.creatorID && this.props.userID !== '') {
            buttons =
                <ButtonToolbar className="owner-buttons">
                    <Button
                        onClick={ this.editPoll.bind(this, this.props.index) }
                    >
                        Edit
                    </Button>
                    <Button
                        onClick={ this.confirmDeletePoll.bind(this, this.props.index) }
                    >
                        Delete
                    </Button>
                </ButtonToolbar>;
        }
        else {
            buttons = null;
        }
        return (
            <div>{buttons}</div>

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        showConfirmModal: (index) => {
            dispatch(showConfirmModal(index));
        },
        showEditModal: (data) => {
            dispatch(showEditModal(data));
        }
    }
};

const OwnerButtons = connect(mapStateToProps, mapDispatchToProps)(_OwnerButtons);

exports.OwnerButtons = OwnerButtons;
