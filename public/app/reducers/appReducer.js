/**
 * Created by Patrick on 19/07/2016.
 */
const initState = {
    showCreateNewUserModal: false,
    showCreateNewPollModal: false,
    showLogInModal: false,
    loggedIn: false,
    signedUp: false,
    createdPoll: false
};

const appReducer = (state = initState, action) => {
    switch (action.type) {
        case 'SHOW-CREATE-NEW-USER-MODAL':
        case 'HIDE-CREATE-NEW-USER-MODAL':
            return Object.assign({}, state, {
                showCreateNewUserModal: !state.showCreateNewUserModal,
                showCreateNewPollModal: false,
                showLogInModal: false
            });
        case 'SHOW-CREATE-NEW-POLL-MODAL':
        case 'HIDE-CREATE-NEW-POLL-MODAL':
            return Object.assign({}, state, {
                showCreateNewPollModal: !state.showCreateNewPollModal,
                showCreateNewUserModal: false,
                showLogInModal: false
            });
        case 'SHOW-LOGIN-MODAL':
        case 'HIDE-LOGIN-MODAL':
            return Object.assign({}, state, {
                showLogInModal: !state.showLogInModal,
                showCreateNewUserModal: false,
                showCreateNewPollModal: false
            });
        case 'LOGGED-IN':
            return Object.assign({}, state, {
                loggedIn: true
            });
        case 'SIGNED-UP':
            return Object.assign({}, state, {
                signedUp: true
            });
        case 'LOGGED-OUT':
            return Object.assign({}, state, {
                loggedIn: false,
                form: 'logIn'
            });
        case 'SHOW-ERROR':
            return Object.assign({}, state, {
                error: action.message
            });
        case 'CREATED-POLL':
            return Object.assign({}, state, {
                createdPoll: true
            });
        default:
            return state
    }
};

exports.appReducer = appReducer;