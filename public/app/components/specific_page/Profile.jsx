const React = require('react');
import { Accordion, Col } from 'react-bootstrap';
import { dataReceived, openPanel, showEditModal, hideEditModal, addOption, showConfirmModal, hideConfirmModal, retrievingData, retrievingDone, removeOption, editValue, resetReducer } from '../../actions/actions';
import { connect } from 'react-redux';
import { VotingPanel } from './../VotingPanel.jsx';
import { EditPollModal } from './../modals/EditPollModal.jsx';
import { ConfirmDeletePollModal } from './../modals/ConfirmDeletePollModal.jsx';
import { WellAlert } from '../WellAlert.jsx';

const _Profile = React.createClass({
    componentDidMount() {
        //resetReducer is needed because resultReducer is also shared with All.jsx
        this.props.resetReducer();
        this.retrieveUserData();
    },
    retrieveUserData() {
        if (!this.props.state.resultReducer.retrieving) {
            this.props.retrievingData();
            if (window.localStorage.user_id !== undefined) {
                console.log('retrieve all user polls called');
                const userID = window.localStorage.user_id;
                $.ajax({
                    url: '/api?userID='+userID,
                    type: 'GET',
                    success: (data) => {
                        // console.log('user polls found, data: ');
                        // console.log(data);
                        this.props.dataReceived(data);
                        this.props.retrievingDone();
                    },
                    error: (err) => {
                        console.log('could not retrieve user data');
                        console.log(err);
                    }
                });
            }
        }
    },
    render: function () {
        // console.log('rendering Profile');
        let data = this.props.state.resultReducer.data;
        let noPollsMessage;
        if (this.props.state.resultReducer.data.length === 0) {
            noPollsMessage = <WellAlert showWell={true} wellStyle={'info'} wellMessage={'You have not created any polls yet'}/>
        }
        let modal;
        if (this.props.state.resultReducer.showEditModal) modal = <EditPollModal />;
        if (this.props.state.resultReducer.showConfirmModal) modal = <ConfirmDeletePollModal />;
        return (
            <div id="profile-results-container">
                <div id="main-title">
                    <h1>My polls</h1>
                </div>
                <Col xs={12} sm={12} md={12} lg={6} lgOffset={3}  >
                    
                    { noPollsMessage }
                    <Accordion>
                        {data.map((item, i) => {
                            return (
                                <VotingPanel data={item} index={i} key={'voting-panel-'+i} parent={'profile'}/>
                            )
                        })}
                    </Accordion>
                    { modal }
                </Col>

            </div>
            
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetReducer: () => {
            dispatch(resetReducer());
        },
        dataReceived: (data) => {
            dispatch(dataReceived(data));
        },
        openPanel: (newPanel) => {
            dispatch(openPanel(newPanel));
        },
        showEditModal: (data) => {
            dispatch(showEditModal(data));
        },
        addOption: () => {
            dispatch(addOption())
        },
        removeOption: (index) => {
            // console.log('remove option in dispatch props, index: '+index);
            dispatch(removeOption(index, 'edit'));
        },
        newOptionValue: (index) => {
            let value =  document.getElementById('pollOption-'+index).value;
            dispatch(editValue(index, value, 'edit'));
        },
        hideEditModal: () => {
            dispatch(hideEditModal());
        },
        showConfirmModal: (index) => {
            dispatch(showConfirmModal(index));
        },
        hideConfirmModal: () => {
            dispatch(hideConfirmModal());
        },
        retrievingData: () => {
            dispatch(retrievingData());
        },
        retrievingDone: () => {
            dispatch(retrievingDone());
        }
    }
};

let Profile = connect(mapStateToProps, mapDispatchToProps)(_Profile);

module.exports = Profile;