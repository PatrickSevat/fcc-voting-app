import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { hideConfirmModal, dataReceived } from '../../actions/actions';

const _ConfirmModal = React.createClass({
    deletePoll() {
        let i = this.props.state.resultReducer.indexToDelete;
        let pollData = this.props.state.resultReducer.data[i];
        let pollID = pollData._id;
        $.ajax({
            url: '/api?pollID='+pollID,
            type: 'DELETE',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', window.localStorage.token);
            },
            success: (data) => {
                console.log('user poll deleted, data: ');
                console.log(data);
                let newData = [...this.props.state.resultReducer.data.slice(0,i),
                    ...this.props.state.resultReducer.data.slice(i+1)
                ];
                console.log('newData');
                console.log(newData);
                this.props.dataReceived(newData);
                setTimeout(this.props.hideConfirmModal, 500);
            },
            error: (err) => {
                console.log('could not delete user poll');
                console.log(err);
            }
        });
    },
   render: function () {
       return (
           <Modal show={this.props.state.resultReducer.showConfirmModal} onHide={this.props.hideConfirmModal}>
               <Modal.Header closeButton>
                   <Modal.Title>Are you sure you want to delete this poll?</Modal.Title>
               </Modal.Header>
               <Modal.Body>
                   <Button id="add-option"
                           onClick={this.deletePoll}
                   >
                       Delete
                   </Button>
                   <Button
                       onClick={this.props.hideConfirmModal}
                   >
                       Cancel
                   </Button>
               </Modal.Body>
           </Modal>
       )
   }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        dataReceived: (data) => {
            dispatch(dataReceived(data));
        },
        hideConfirmModal: () => {
            dispatch(hideConfirmModal());
        }
    }
};

const ConfirmModal = connect(mapStateToProps, mapDispatchToProps)(_ConfirmModal);

exports.ConfirmDeletePollModal = ConfirmModal;

