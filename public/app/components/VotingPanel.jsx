const React = require('react');
import { Panel, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { openPanel, addVote } from '../actions/actions';
import { DonutChart } from './DonutChart.jsx';
import { OwnerButtons } from './OwnerButtons.jsx';

const _VotingPanel = React.createClass({
    render: function () {
        // console.log('rendering Voting Panel, parent: '+this.props.parent);
        let openPanel = this.props.state.resultReducer.openPanel;
        let data;
        if (this.props.parent === 'search') {
            data = this.props.data
        }
        else {
            if (this.props.state.resultReducer.data instanceof Array) {
                data = this.props.state.resultReducer.data[this.props.index];
            }
            else {
                data = this.props.state.resultReducer.data;
            }
        }
        // console.log('data in render:'); 
        // console.log(data);
        let i = this.props.index;
        let userID = '';
        let creatorID = '';
        let ownerButtons = '';
        let panelBody;
        //Check for search parent needed because search is server side rendered
        // and does not know of log in buttons, therefore leading to mismatch between server side and client side
        // TODO give server side rendering knowledge of login
        if (typeof window !== 'undefined' && this.props.parent !== 'search') {
            userID = window.localStorage.user_id;
            creatorID = data.creatorID;
            ownerButtons = <OwnerButtons userID={userID} creatorID={creatorID} index={i} />
        }
        if (openPanel === i) {
            panelBody =
                <div className={"panelBody"} >
                    <div className="pollChart">
                        <DonutChart data={data} index={i} />
                    </div>
                    <div className="pollControls">
                        <p className="description">Description: {data.description}</p>
                        <p className="username">Created by: {data.username}</p>
                            {data.options.map((option, j) => {
                                return (
                                    <div key={'option-'+j} className="voting-Button">
                                        <Button
                                            className="voting-Button"
                                            id={"voting-button-"+i+'-'+j}
                                            onClick={
                                                        this.props.addVote.bind(this, j, data, i)
                                                        }
                                        >Option: {option.value} ({option.optionVotes} votes)</Button>
                                    </div>
                                )
                            })
                            }
                    </div>

                    {ownerButtons}
                </div>

        }
        return (
            <Panel header={data.name}
                   collapsible
                   expanded={openPanel === i}
                   onSelect={ this.props.openPanel.bind(this, i) }
                   key={'poll-'+i}
                   eventKey={i}>
                {panelBody}
            </Panel>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        openPanel: (newPanel) => {
            dispatch(openPanel(newPanel));
        },
        addVote: (optionIndex, data, dataIndex) => {
            // TODO prohibit clicking until return animation is complete
            var id = document.getElementById('voting-button-'+dataIndex+'-'+optionIndex);
            // console.log('animating: '+id);
            const returnAnimation = () => {
                animation.reverse();
                document.activeElement.blur();
            };
            var animation = TweenMax.to(id, 0.5, {
                scale: 1.25,
                onComplete: returnAnimation
            });

            data.options[optionIndex].optionVotes = Number(data.options[optionIndex].optionVotes)+1;
            $.ajax({
                url: '/api/vote?pollID='+data._id,
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', window.localStorage.token);
                },
                data: {options: data.options},
                success: (data) => {
                    // console.log('voted succes, new data: ');
                    // console.log(data);
                    dispatch(addVote(data));
                },
                error: (err) => {
                    console.log('voting failed: err');
                    console.log(err);
                }
            });

        }
    }
};

const Votingpanel = connect(mapStateToProps, mapDispatchToProps)(_VotingPanel);

exports.VotingPanel = Votingpanel;