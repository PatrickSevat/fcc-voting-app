import React from 'react';
import { Modal, ControlLabel, FormControl, FormGroup, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { hideCreateNewPollModal, addOption, removeOption, editValue, createdPoll, showError, dataReceived, retrievingData, retrievingDone } from '../../actions/actions';
import { PollOptions } from '../PollOptions.jsx';
import { WellAlert } from '../WellAlert.jsx';

const _CreateModal = React.createClass({
    retrieveUserData() {
        if (!this.props.state.resultReducer.retrieving) {
            this.props.retrievingData();
            if (window.localStorage.user_id !== undefined) {
                const userID = window.localStorage.user_id;
                $.ajax({
                    url: '/api?userID='+userID,
                    type: 'GET',
                    success: (data) => {
                        // console.log('user polls found, data: ');
                        // console.log(data);
                        this.props.dataReceived(data);
                        this.props.retrievingDone();
                        return data;
                    },
                    error: (err) => {
                        console.log('could not retrieve user data');
                        console.log(err);
                    }
                });
            }
        }
    },
    submitPoll() {
        let creatorID = window.localStorage.user_id;
        let username = window.localStorage.username;
        let pollName = document.getElementById('pollName').value;
        let pollDescription = document.getElementById('pollDescription').value;
        let options = this.props.state.pollsReducer.newPollOptions;
        let dataToSend = { creatorID, username, pollName, pollDescription, options };
        if (options.length === 0) {
            console.log('ERROR OPTIONS REQUIRED')
        }
        else {
            $.ajax({
                url: '/api?createPoll=true',
                type: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', window.localStorage.token);
                },
                data: dataToSend,
                success: (data) => {
                    console.log('created poll successful, data: ');
                    console.log(data.message);
                    console.log('data.data');
                    console.log(data.data);
                    this.props.createdPoll(data.data);
                    this.retrieveUserData();
                    setTimeout(this.props.hideCreateNewPollModal, 1000);
                },
                error: (err) => {
                    console.log('create poll failed: err');
                    console.log(err);
                    this.props.ShowError(err);
                }
            });
        }
        
    },
    render: function () {
        // console.log('rendering create new poll Modal');
        let options = [];
        this.props.state.pollsReducer.newPollOptions.map((item, i) => {
            options.push(<PollOptions
                index={i} 
                data={item}
                key={'poll-option-'+i}
                modal={"create"}
            />)
        });
        return (
            <Modal show={this.props.state.appReducer.showCreateNewPollModal} onHide={this.props.hideCreateNewPollModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Create a poll</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form id="create-poll" >
                        <FormGroup>
                            <ControlLabel>Poll name</ControlLabel>
                            <FormControl type="text" id="pollName" name="pollName" />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Description</ControlLabel>
                            <FormControl type="text" id="pollDescription" name="pollDescription" />
                        </FormGroup>
                        { this.props.state.pollsReducer.newPollOptions.map((item, i) => {
                            return(<PollOptions index={i} data={item} modal="create" key={'poll-option-'+i}/>)
                        }) }
                        <Button id="add-option"
                                onClick={this.props.addOption}
                        >
                            Add another option
                        </Button>
                        <Button id="create-submit"
                                onClick={this.submitPoll}
                        >
                            Create
                        </Button>
                        <Button
                            onClick={this.props.hideCreateNewPollModal}
                        >
                            Close
                        </Button>

                    </form>
                </Modal.Body>
            </Modal>
        )
    }
});


const mapStateToProps = (state) => {
    return {state}
};
const mapDispatchToProps = (dispatch) => {
    return {
        dataReceived: (data) => {
            dispatch(dataReceived(data));
        },
        retrievingData: () => {
            dispatch(retrievingData());
        },
        retrievingDone: () => {
            dispatch(retrievingDone());
        },
        hideCreateNewPollModal: () => {
            dispatch(hideCreateNewPollModal())
        },
        addOption: () => {
            dispatch(addOption())
        },
        removeOption: (index) => {
            // console.log('remove option in dispatch props');
            dispatch(removeOption(index, 'create'));
        },
        newOptionValue: (index) => {
            let value =  document.getElementById('pollOption-'+index).value;
            dispatch(editValue(index, value, 'create'));

        },
        createdPoll: (data) => {
            console.log('dispatching createdPoll');
            dispatch(createdPoll(data))
        },
        ShowError: (err) => {
            dispatch(showError(err))
        }
    }
};

const CreateModal = connect(mapStateToProps, mapDispatchToProps)(_CreateModal);

exports.CreateNewPollModal = CreateModal;