/**
 * Created by Patrick on 19/07/2016.
 */
export const logIn = () => {
    return {
        type: 'LOGGED-IN'
    }
};

export const signUp = () => {
    return {
        type: 'SIGNED-UP'
    }
};

export const logOut = () => {
    return {
        type: 'LOGGED-OUT'
    }
};

export const showCreateNewUserModal = () => {
    return {
        type: 'SHOW-CREATE-NEW-USER-MODAL'
    }
};

export const hideCreateNewUserModal = () => {
    return {
        type: 'HIDE-CREATE-NEW-USER-MODAL'
    }
};

export const showCreateNewPollModal = () => {
    return {
        type: 'SHOW-CREATE-NEW-POLL-MODAL'
    }
};

export const hideCreateNewPollModal = () => {
    return {
        type: 'HIDE-CREATE-NEW-POLL-MODAL'
    }
};

export const showLogInModal = () => {
    return {
        type: 'SHOW-LOGIN-MODAL'
    }
};

export const hideLogInModal = () => {
    return {
        type: 'HIDE-LOGIN-MODAL'
    }
};

export const showError = (err) => {
    return {
        type: 'SHOW-ERROR',
        message: err
    }
};

export const addOption = () => {
    return {
        type: 'ADD-OPTION'
    }
};

export const createdPoll = (pollData) => {
    return {
        type: 'CREATED-POLL',
        pollData
    }
};

export const dataReceived = (data) => {
    return {
        type: 'DATA-RECEIVED',
        data: data
    }
};

export const openPanel = (newPanel) => {
    return {
        type: 'OPEN-PANEL',
        newPanel
    }
};

export const showEditModal = (data) => {
    return {
        type: 'SHOW-EDIT-MODAL',
        data
    }
};

export const hideEditModal = () => {
    return {
        type: 'HIDE-EDIT-MODAL'
    }
};

export const showConfirmModal = (i) => {
    return {
        type: 'SHOW-CONFIRM-MODAL',
        indexToDelete: i
    }
};

export const hideConfirmModal = () => {
    return {
        type: 'HIDE-CONFIRM-MODAL'
    }
};

export const retrievingData = () => {
    return {
        type: 'RETRIEVING-DATA'
    }
};

export const retrievingDone = () => {
    return {
        type: 'RETRIEVING-DONE'
    }
};

export const removeOption = (index, form) => {
    return {
        type: 'REMOVE-OPTION',
        index,
        form
    }
};

export const editValue = (index, value, form) => {
    return {
        type: 'EDIT-VALUE',
        index,
        value,
        form
    }  
};

export const resetReducer = () => {
    return {
        type: 'RESET-REDUCER'
    }
};

export const addVote = (data) => {
    console.log('addvote action dispatched');
    return {
        type: 'ADD-VOTE',
        data
    }
};
