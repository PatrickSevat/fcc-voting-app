import React from 'react';
import { Modal, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { showEditModal, addOption, removeOption, editValue, hideEditModal, dataReceived, openPanel } from '../../actions/actions';
import { PollOptions } from '../PollOptions.jsx';

const _EditModal = React.createClass({
    submitEditedData() {
        let pollID = this.props.state.resultReducer.editModalData._id;
        let pollName = document.getElementById('pollName').value;
        let pollDescription = document.getElementById('pollDescription').value;
        let options = [];
        for (let i = 0; i < this.props.state.resultReducer.editModalData.options.length; i++) {
            options.push({
                value: document.getElementById('pollOption-'+i).value,
                optionVotes: this.props.state.resultReducer.editModalData.options[i].optionVotes
            })
        }
        let data = {  pollName, pollDescription, options };
        if (options.length === 0) {
            // console.log('ERROR CANNOT SAVE WITHOUT OPTIONS');
        }
        else {
            $.ajax({
                url: '/api?pollID='+pollID,
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', window.localStorage.token);
                },
                data: data,
                success: (data) => {
                    // console.log('user poll updated, data: ');
                    // console.log(data);
                    for (let i = 0; i<this.props.state.resultReducer.data.length; i++) {
                        if (this.props.state.resultReducer.data[i]._id === data._id) {
                            let newData = [...this.props.state.resultReducer.data.slice(0,i),
                                data,
                                ...this.props.state.resultReducer.data.slice(i+1)
                            ];
                            this.props.dataReceived(newData);
                            this.props.openPanel(i);
                            this.props.hideEditModal()
                        }
                    }
                },
                error: (err) => {
                    console.log('could not update user poll');
                    console.log(err);
                }
            });
        }
        
    },
   render: function () {
       // console.log('rendering Edit Modal');
       let options = [];
       if (this.props.state.resultReducer.editModalData) {
           let numOptions = this.props.state.resultReducer.editModalData.options.length;
           this.props.state.resultReducer.editModalData.options.map((item, i) => {
              options.push(<PollOptions 
                  index={i} 
                  data={item} 
                  modal="edit"
                  numOptions={numOptions}
                  key={'poll-option-'+i}
              />)
           });
       }
       return (
           <Modal show={this.props.state.resultReducer.showEditModal} onHide={this.props.hideEditModal}>
               <Modal.Header closeButton>
                   <Modal.Title>Edit poll</Modal.Title>
               </Modal.Header>
               <Modal.Body>
                   <form id="edit-poll" >
                       <FormGroup>
                           <ControlLabel>Poll name</ControlLabel>
                           <FormControl type="text" id="pollName" name="pollName" defaultValue={this.props.state.resultReducer.editModalData.name} />
                       </FormGroup>
                       <FormGroup>
                           <ControlLabel>Description</ControlLabel>
                           <FormControl type="text" id="pollDescription" name="pollDescription" defaultValue={this.props.state.resultReducer.editModalData.description} />
                       </FormGroup>
                       { options }
                       <Button id="add-option"
                               onClick={this.props.addOption}
                       >
                           Add another option
                       </Button>
                       <Button id="edit-submit"
                               onClick={this.submitEditedData}
                       >
                           Save
                       </Button>
                       <Button
                           onClick={this.props.hideEditModal}
                       >
                           Close
                       </Button>

                   </form>
               </Modal.Body>
           </Modal>
       )
   } 
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        dataReceived: (data) => {
            dispatch(dataReceived(data));
        },
        showEditModal: (data) => {
            dispatch(showEditModal(data));
        },
        addOption: () => {
            dispatch(addOption())
        },
        removeOption: (index) => {
            // console.log('remove option in dispatch props, index: '+index);
            dispatch(removeOption(index, 'edit'));
        },
        newOptionValue: (index) => {
            let value =  document.getElementById('pollOption-'+index).value;
            dispatch(editValue(index, value, 'edit'));
        },
        hideEditModal: () => {
            dispatch(hideEditModal());
        },
        openPanel: (newPanel) => {
            dispatch(openPanel(newPanel));
        }
    }
};

const EditModal = connect(mapStateToProps, mapDispatchToProps)(_EditModal);

exports.EditPollModal = EditModal;