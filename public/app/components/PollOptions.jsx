import React from 'react';
import { FormGroup, ControlLabel, Button, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import { removeOption, editValue} from '../actions/actions';

const _PollOptions = React.createClass({
    render: function () {
        // console.log('rendering Poll Options');
        let i = this.props.index;
        let value;
        if (this.props.modal === 'create') {
            value = this.props.state.pollsReducer.newPollOptions[i].value;
        }
        else if (this.props.modal === 'edit') {
            value = this.props.state.resultReducer.editModalData.options[i].value;
        }
        return (
            <FormGroup key={'option-formgroup-'+i}>
                <ControlLabel>
                    {'Option '}
                        <span className="pull-right">
                        <Button
                            onClick={ this.props.removeOption.bind(this, i) }>
                            Remove option
                        </Button>
                    </span>
                </ControlLabel>
                <FormControl
                    type="text"
                    id={"pollOption-"+i}
                    name="pollOption"
                    value={value || ''}
                    onChange={this.props.newOptionValue.bind(this, i)}
                />
            </FormGroup>
        )
    }
    
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = function (dispatch) {
    return {
        removeOption: function (index) {
            dispatch(removeOption(index, this.props.modal));
        },
        newOptionValue: function (index) {
            let value =  document.getElementById('pollOption-'+index).value;
            dispatch(editValue(index, value, this.props.modal));
        }
    }
};

const PollOptions = connect(mapStateToProps, mapDispatchToProps)(_PollOptions);

exports.PollOptions = PollOptions;
